#!/usr/bin/env python3

import argparse
import asyncio
import signal
import time
from websockets import serve  # type: ignore
from websockets.server import WebSocketServerProtocol  # type: ignore


continue_running = True
verbose = False


def sig_handler(signum, frame):
    global continue_running
    continue_running = False


def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-i", "--interface", type=str, default="localhost", help="Interface address to receive messages onto")
    parser.add_argument("-p", "--port", type=int, default=4435, help="Port to receive onto")
    parser.add_argument("-v", "--verbose", type=bool, default=False, help="Print everything received")
    return parser.parse_known_args()[0]


async def ws_loop(ws_server: WebSocketServerProtocol, path: str):
    print(f"Connection established from {ws_server.remote_address[0]}:{ws_server.remote_address[1]}")

    while continue_running:
        new_data = await(ws_server.recv())
        if verbose:
            print(new_data)

    print(f"Connection from {ws_server.remote_address[0]}:{ws_server.remote_address[1]} closed")


async def ws_worker(interface: str, port: int):
    async with serve(ws_loop, interface, port, ssl=None):
        print(f"Listening for Websocket connections through ws://{interface}:{port}")
        while continue_running:
            await asyncio.sleep(0.1)


if __name__ == "__main__":
    signal.signal(signal.SIGTERM, sig_handler)
    signal.signal(signal.SIGINT, sig_handler)

    args = parse_args()
    verbose = args.verbose

    asyncio.run(ws_worker(args.interface, args.port))

    while continue_running:
        time.sleep(1)
